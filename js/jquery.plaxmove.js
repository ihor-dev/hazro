(function($){
    $.fn.plaxmove = function(options) {

        this.defaults = {
            ratioH: 0.2,
            ratioV: 0.2,
            reversed: false
        }

        var settings = $.extend({},this.defaults,options),
            layer = $(this),
            center = {
                x: $('html').width()/2,
                y: $('html').height()/2
            },
            y0 = layer.offset().top,
            x0 = layer.offset().left;
        
        //Инвариант процесса: при установке указателя мыши в точку center все элементы возвращаются на место
        var eqH = function(e) {
            return x0 + (e.pageX - center.x)*settings.ratioH
        }

        var eqW = function(e) {
            return y0 + (e.pageY - center.y)*settings.ratioV
        }        

        if(settings.reversed) {
                var t = eqH; eqH = eqW; eqW = t;
        }
            
        //Данные свойства css иногда помогают указать начальное расположение элемента, но при движении мыши они будут только мешать
        layer.css('margin-left', '0px')
        layer.css('margin-top', '0px')
        layer.css({top:y0, left:x0})

        $('html').on('mousemove', function(e) {
                x = eqH(e)
                y = eqW(e)
                layer.css({top:y,left:x})
        })

    };
})(jQuery);

