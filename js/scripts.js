

$(document).ready(function(){              
        //main page tabs hover
        $(".hovered").hover(
			function(){
				var id = $(this).attr('id');
				var overlay = $('#'+id+' .product-hover');
				var p = $('#'+id+' p, #'+id+' b');
				overlay.css({'visibility': 'visible'});
				p.css({'color':'#ddd'});
				overlay.html(p.attr('title')+'<br><hr>');
			},
			function(){
				var id = $(this).attr('id');
				var overlay = $('#'+id+' .product-hover');
				var p = $('#'+id+' p, #'+id+' b');
				overlay.css({'visibility': 'hidden'});
				p.css({'color':'#5d5f64'});
			}
		);
		
		$(".dropdown a").popover({"html":true, 'animation':false});
		
		$(".dropdown a").on('shown.bs.popover', function () {
			var width = $(document).width();
			
			if(width < 751) {$('.popover').css('display','none');return false;}
			
			var type = $(".open").attr('id');
			if(type == "accessories"){			
				var h = 158;

			}else {
				var h = $(".open > ul.dropdown-menu").height();
			}
			
			$(".popover-content img").css({"height":h+'px'});
			
			var left = parseInt($(".popover").css('left').replace('px',''));
			
			$(".popover").css({'left': left-10+'px', 'box-shadow':'none','border-radius':'0px'});
			$('.arrow').hide();
			
		});
		
		//logo url 
		$("img[src$='logo.png']").click(function(){						
			if($(this).attr('data') == "main-page") {
				location.href = "./index.html";
			}else{
				location.href = "../index.html";
			}
			
		});
		
		
		//product modal
		if($("#product_slider .royalSlider").width()!==null){
			$("#product_slider .royalSlider").on('click', '.rsSlide', function(){
				var index = $(this).index();
				
				//initial modal
				$(".colorbox").colorbox({open:true,  maxHeight: '100%', maxWidth: '100%' ,index: index});
				$(document).scrollTop();
				return false;
			});
		}
		

});
